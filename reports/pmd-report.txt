src/controller/SimulationLoop.java:56:	Avoid printStackTrace(); use a logger call instead.
src/controller/SimulationLoop.java:67:	Avoid printStackTrace(); use a logger call instead.
src/model/simulator/bacteria/ActionManager.java:51:	Avoid printStackTrace(); use a logger call instead.
src/model/simulator/bacteria/ActionPerformerImpl.java:142:	Avoid printStackTrace(); use a logger call instead.
src/model/simulator/bacteria/ActionPerformerImpl.java:173:	Avoid printStackTrace(); use a logger call instead.
src/model/simulator/bacteria/ActionPerformerImpl.java:221:	Avoid printStackTrace(); use a logger call instead.
src/view/model/food/CreationViewFoodImpl.java:135:	Avoid instantiation through private constructors from outside of the constructors class.
